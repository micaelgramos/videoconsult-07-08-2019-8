#import <Cordova/CDVPlugin.h>
//@Noesis 2019.10.31
//#import <Cordova/CDV.h>

@class  VidyoViewController;

@interface VidyoIOPlugin : CDVPlugin

@property (nonatomic, retain) VidyoViewController* vidyoViewController;

- (void)launchVidyoIO:(CDVInvokedUrlCommand *)command;

- (void)destroy;

@end







